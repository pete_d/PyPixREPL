// TODO this code sucks organize it better like move all the rendering module
// code to a different file and tidy up the loop and stuff

#define _POSIX_C_SOURCE 200112L

// "Note: Since Python may define some pre-processor definitions which affect
// the standard headers on some systems, you must include Python.h before any
// standard headers are included."
#include <Python.h>
#include <SDL.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

static SDL_Window *window = NULL;
static int window_width, window_height;
static SDL_Renderer *renderer;
static int py_render_enabled = 0;

struct commandline {
    char buf[40960];
    size_t buf_index;
    int continuing;
};

static int read_commandline(struct commandline *cl)
{
    int count;
    size_t old_index;

    count = read(0, cl->buf + cl->buf_index, sizeof(cl->buf) - cl->buf_index);
    if (count == -1) {
	if (errno != EAGAIN) {
	    fprintf(stderr, "stdin read error: %s\n", strerror(errno));
	}
	return 0;
    }
    old_index = cl->buf_index;
    cl->buf_index += count;
    for (int i = old_index; i < cl->buf_index; i++) {
	if (cl->buf[i] == '\n') {
	    if (i > 0 && cl->buf[i - 1] == ':') {
		printf("... ");
		fflush(stdout);
		cl->continuing = 1;
		return 0;
	    }
	    if (cl->continuing && old_index != cl->buf_index - 1) {
		printf("... ");
		fflush(stdout);
		return 0;
	    }
	    cl->buf[i] = '\0';
	    cl->buf_index = 0;
	    cl->continuing = 0;
	    return 1;
	}
    }
    if (cl->buf_index == sizeof(cl->buf)) {
	printf("command too long, resetting\n");
	count = read(0, cl->buf, sizeof(cl->buf)); // attempt to flush stdin
	cl->buf_index = 0;
    }

    return 0;
}

static PyObject *py_render_enable(PyObject *self, PyObject *args)
{
    py_render_enabled = 1;

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *py_render_disable(PyObject *self, PyObject *args)
{
    py_render_enabled = 0;

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *py_render_clear(PyObject *self, PyObject *args)
{
    SDL_RenderClear(renderer);

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *py_render_set_color(PyObject *self, PyObject *args)
{
    unsigned char r, g, b;

    if (!PyArg_ParseTuple(args, "bbb", &r, &g, &b)) {
	return NULL;
    }

    SDL_SetRenderDrawColor(renderer, r, g, b, 0xff);

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *py_render_line(PyObject *self, PyObject *args)
{
    int x1, y1, x2, y2;

    if (!PyArg_ParseTuple(args, "iiii", &x1, &y1, &x2, &y2)) {
	return NULL;
    }

    SDL_RenderDrawLine(renderer, x1, y1, x2, y2);

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *py_render_point(PyObject *self, PyObject *args)
{
    int x, y;

    if (!PyArg_ParseTuple(args, "ii", &x, &y)) {
	return NULL;
    }

    SDL_RenderDrawPoint(renderer, x, y);

    Py_INCREF(Py_None);
    return Py_None;
}


static PyObject *py_render_ticks(PyObject *self, PyObject *args)
{
    return PyInt_FromLong(SDL_GetTicks());
}

static PyObject *py_render_size(PyObject *self, PyObject *args)
{
    return PyTuple_Pack(2, PyInt_FromLong(window_width), PyInt_FromLong(window_height));
}

static PyMethodDef py_render_methods[] = {
    { "enable", py_render_enable, METH_NOARGS,
      "Enable auto-rendering." },
    { "disable", py_render_disable, METH_NOARGS,
      "Disable auto-rendering." },
    { "clear", py_render_clear, METH_NOARGS,
      "Clear the screen." },
    { "set_color", py_render_set_color, METH_VARARGS,
      "Set renderer color. (r, g, b)" },
    { "line", py_render_line, METH_VARARGS,
      "Draw a line. (x1, y1, x2, y2)" },
    { "point", py_render_point, METH_VARARGS,
      "Draw a point. (x, y))" },
    { "ticks", py_render_ticks, METH_NOARGS,
      "Get milliseconds since start of REPL." },
    { "size", py_render_size, METH_NOARGS,
      "Get width x height of screen." },
    { NULL, NULL, 0, NULL }
};

static void init_python(PyObject **environment_ptr)
{
    PyObject *environment;
    PyObject *render_module;

    Py_SetProgramName("PyPixREPL");
    Py_Initialize();
    environment = PyModule_GetDict(PyImport_AddModule("__main__"));
    render_module = Py_InitModule("render", py_render_methods);
    PyDict_SetItemString(environment, "render", render_module);
    *environment_ptr = environment;
}

int main(int argc, const char *argv[])
{
    int rc = 1;
    SDL_Event event;
    struct commandline cl;
    PyObject *py_environment;

    cl.buf_index = 0;
    cl.continuing = 0;

    // set stdin non-blocking for command line reading
    if (fcntl(0, F_SETFL, O_NONBLOCK) != 0) {
	fprintf(stderr, "couldn't set stdin non-blocking: %s\n", strerror(errno));
	goto out;
    }

    // create SDL
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
	fprintf(stderr, "couldn't init SDL: %s\n", SDL_GetError());
	goto out;
    }

    window = SDL_CreateWindow(
	"PyPixREPL",
	SDL_WINDOWPOS_UNDEFINED,
	SDL_WINDOWPOS_UNDEFINED,
	0,
	0,
	SDL_WINDOW_SHOWN | SDL_WINDOW_FULLSCREEN_DESKTOP);
    if (!window) {
	fprintf(stderr, "couldn't create window: %s\n", SDL_GetError());
	goto out_sdl;
    }

    renderer = SDL_CreateRenderer(
	window,
	-1,
	SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (!renderer) {
	fprintf(stderr, "couldn't create renderer: %s\n", SDL_GetError());
	goto out_window;
    }

    init_python(&py_environment);

    printf(">>> ");
    fflush(stdout);
    for (;;) {
	SDL_GetWindowSize(window, &window_width, &window_height);

	while (SDL_PollEvent(&event) != 0) {
	    switch (event.type) {
	    case SDL_QUIT:
		goto done;
	    case SDL_KEYDOWN:
		if (event.key.keysym.sym == SDLK_q) {
		    goto done;
		}
	    }
	}
	if (read_commandline(&cl)) {
	    if (strcmp(cl.buf, "quit") == 0) {
		goto done;
	    }

	    if (cl.buf[0] != '\0') {
		PyObject *ob = PyRun_String(
		    cl.buf,
		    Py_single_input,
		    py_environment,
		    py_environment);
		if (PyErr_Occurred()) {
		    PyErr_PrintEx(1);
		}
		Py_XDECREF(ob);
	    }

	    printf(">>> ");
	    fflush(stdout);
	}

	if (py_render_enabled) {
	    PyObject *ob = PyDict_GetItemString(py_environment, "renderer");
	    if (ob && PyFunction_Check(ob)) {
		PyObject *result = PyObject_CallObject(ob, NULL);
		if (PyErr_Occurred()) {
		    PyErr_PrintEx(1);
		    printf("Python rendering disabled\n");
		    py_render_enabled = 0;
		}
		Py_XDECREF(result);
	    }
	}
	SDL_RenderPresent(renderer);
    }

  done:
  out_python:
    Py_XDECREF(py_environment);
    Py_Finalize();
  out_renderer:
    SDL_DestroyRenderer(renderer);
  out_window:
    SDL_DestroyWindow(window);
  out_sdl:
    SDL_Quit();
  out:
    return rc;
}
