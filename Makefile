CFLAGS=-g3 -std=c99 -Wall -Wno-unused-label $(shell pkg-config --cflags sdl2 python-2.7)
LDFLAGS=$(shell pkg-config --libs sdl2 python-2.7)

.PHONY: all clean

all: pypixrepl

clean:
	rm -f pypixrepl *.o
