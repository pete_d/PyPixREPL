#!/usr/bin/python

import math

# this can take a while, so by default it skips most pixels
# call with quick=False for full detail
def mandelbrot(quick = True):
    def magnitude(c):
        return math.sqrt(c.real ** 2 + c.imag ** 2)
    def divergence(c):
        z = 0
        for n in range(30):
            z = z ** 2 + c
            if magnitude(z) > 10:
                break
        return n
    (width, height) = render.size()
    render.set_color(0, 0, 0)
    render.clear()
    step = 8 if quick else 1
    for px in range(0, width, step):
        for py in range(0, height, step):
            x = (px - width / 2) / float(min(width, height)) * 2.5 - 0.5
            y = (py - height / 2) / float(min(width, height)) * 2.5
            n = divergence(complex(x, y))
            v = min(n * 10, 255)
            render.set_color(v, v, v)
            render.point(px, py)

mandelbrot()
