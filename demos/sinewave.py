#!/usr/bin/python
# a sine-wave demo to show off auto-rendering

import math

def renderer():
    t = render.ticks()
    (width, height) = render.size()
    def f(x):
        return height / 2 + int(math.sin(x/100.0 + t/500.0) * (height / 2))
    render.set_color(255, 255, 255)
    render.clear()
    render.set_color(0, 0, 0)
    for x in range(width - 1):
        render.line(x, f(x), x + 1, f(x + 1))

render.enable()
